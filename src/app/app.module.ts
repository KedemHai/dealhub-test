import { BrowserModule } from '@angular/platform-browser'
import { Injector, NgModule } from '@angular/core'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { StudentManagementComponent } from './student-management/student-management.component'
import { StudentSummaryComponent } from './student-summary/student-summary.component'
import { ReactiveFormsModule } from '@angular/forms'
import {
  CLASS_STUDENTS_CLASS_TYPES_GATEWAY,
  CLASS_STUDENTS_STUDENTS_GATEWAY,
  CLASS_STUDENTS_YEARS_GATEWAY,
  SUMMARIZE_CLASS_STUDENTS,
  UPDATE_STUDENT_SCORE
} from './tokens'
import SummarizeClassStudents from '../../Dealhub/App/SummarizeClassStudents/SummarizeClassStudents'
import InMemoryClassStudentsYearsGateway from '../../Dealhub/Infrastructure/InMemory/InMemoryClassStudentsYearsGateway'
import InMemoryClassStudentsClassTypesGateway
  from '../../Dealhub/Infrastructure/InMemory/InMemoryClassStudentsClassTypesGateway'
import InMemoryClassStudentsStudentsGateway
  from '../../Dealhub/Infrastructure/InMemory/InMemoryClassStudentsStudentsGateway'
import { ClassStudentsYearsGateway } from '../../Dealhub/App/Gateways/ClassStudentsYearsGateway'
import { ClassStudentsClassTypesGateway } from '../../Dealhub/App/Gateways/ClassStudentsClassTypesGateway'
import { ClassStudentsStudentsGateway } from '../../Dealhub/App/Gateways/ClassStudentsStudentsGateway'
import { StudentDetailsComponent } from './student-details/student-details.component'
import { EditScoreComponent } from './edit-score/edit-score.component'
import UpdateStudentScore from '../../Dealhub/App/UpdateStudentScore/UpdateStudentScore'
import { StudentManagementService } from './student-management.service'

@NgModule({
  declarations: [
    AppComponent,
    StudentManagementComponent,
    StudentSummaryComponent,
    StudentDetailsComponent,
    EditScoreComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: SUMMARIZE_CLASS_STUDENTS,
      useFactory: (injector: Injector) => {
        const yearsGateway: ClassStudentsYearsGateway = injector.get(CLASS_STUDENTS_YEARS_GATEWAY)
        const classTypesGateway: ClassStudentsClassTypesGateway = injector.get(CLASS_STUDENTS_CLASS_TYPES_GATEWAY)
        const studentsGateway: ClassStudentsStudentsGateway = injector.get(CLASS_STUDENTS_STUDENTS_GATEWAY)
        return new SummarizeClassStudents(yearsGateway, classTypesGateway, studentsGateway)
      },
      deps: [Injector]
    },
    {
      provide: CLASS_STUDENTS_YEARS_GATEWAY,
      useFactory: () => {
        return new InMemoryClassStudentsYearsGateway()
      }
    },
    {
      provide: CLASS_STUDENTS_CLASS_TYPES_GATEWAY,
      useFactory: () => {
        return new InMemoryClassStudentsClassTypesGateway()
      }
    },
    {
      provide: CLASS_STUDENTS_STUDENTS_GATEWAY,
      useFactory: () => {
        return new InMemoryClassStudentsStudentsGateway()
      }
    },
    {
      provide: UPDATE_STUDENT_SCORE,
      useFactory: (injector: Injector) => {
        const studentGateway: ClassStudentsStudentsGateway = injector.get(CLASS_STUDENTS_STUDENTS_GATEWAY)
        return new UpdateStudentScore(studentGateway)
      },
      deps: [Injector]
    },
    StudentManagementService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
