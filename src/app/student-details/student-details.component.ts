import { Component, HostBinding, Injector, Input } from '@angular/core'
import { ClassStudent } from '../../../Dealhub/Domain/ClassStudent'
import {
  UpdateStudentScoreInput,
  UpdateStudentScoreOutput
} from '../../../Dealhub/App/UpdateStudentScore/UpdateStudentScoreContract'
import { UPDATE_STUDENT_SCORE } from '../tokens'
import UpdateStudentScoreRequest from '../../../Dealhub/App/UpdateStudentScore/UpdateStudentScoreRequest'
import { StudentManagementService } from '../student-management.service'

@Component({
  selector: 'dealhub-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.scss']
})
export class StudentDetailsComponent implements UpdateStudentScoreOutput {
  @Input() public student?: ClassStudent
  private readonly updateStudentScore: UpdateStudentScoreInput
  private readonly studentManagementService: StudentManagementService

  @HostBinding('class.low-score')
  private get hasLowScore (): boolean {
    if (typeof this.student === 'undefined') {
      return false
    }

    return this.student.grade <= 45
  }

  constructor (injector: Injector, studentManagementService: StudentManagementService) {
    this.updateStudentScore = injector.get<UpdateStudentScoreInput>(UPDATE_STUDENT_SCORE)
    this.studentManagementService = studentManagementService
  }

  studentScoreUpdated ($event: number): void {
    const request = new UpdateStudentScoreRequest()
    request.studentId = this.student?.id
    request.score = $event
    this.updateStudentScore.execute(request, this)
  }

  displayUpdateScoreFail (message: string): void {
    throw new Error(message)
  }

  displayUpdateScoreSuccess (student: ClassStudent): void {
    this.studentManagementService.studentHasUpdated.next(student)
  }
}
