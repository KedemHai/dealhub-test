import {
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  ComponentRef,
  EventEmitter,
  Inject,
  Injector,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core'
import {
  StudentSummaryUserActions,
  StudentSummaryView
} from '../../../Dealhub/UI/StudentSummary/StudentSummaryViewContract'
import {
  SummarizeClassStudentsInput,
  SummarizeClassStudentsOutput
} from '../../../Dealhub/App/SummarizeClassStudents/SummarizeClassStudentsContract'
import { SUMMARIZE_CLASS_STUDENTS, UPDATE_STUDENT_SCORE } from '../tokens'
import StudentSummaryPresenter from '../../../Dealhub/UI/StudentSummary/StudentSummaryPresenter'
import AngularStudentSummaryView from './AngularStudentSummaryView'
import { FormControl } from '@angular/forms'
import { ClassStudent } from '../../../Dealhub/Domain/ClassStudent'
import { StudentDetailsComponent } from '../student-details/student-details.component'
import { StudentManagementService } from '../student-management.service'
import {
  UpdateStudentScoreInput,
  UpdateStudentScoreOutput
} from '../../../Dealhub/App/UpdateStudentScore/UpdateStudentScoreContract'
import UpdateStudentScoreRequest from '../../../Dealhub/App/UpdateStudentScore/UpdateStudentScoreRequest'

function yearAsNumber (year: string | null): number | null {
  if (year === null) {
    return year
  }

  return Number(year)
}

@Component({
  selector: 'dealhub-student-summary',
  templateUrl: './student-summary.component.html',
  styleUrls: ['./student-summary.component.scss']
})
export class StudentSummaryComponent implements StudentSummaryUserActions, OnInit, UpdateStudentScoreOutput {
  private readonly injector: Injector
  private readonly useCase: SummarizeClassStudentsInput
  private readonly presenter: SummarizeClassStudentsOutput
  private readonly view: StudentSummaryView
  private readonly studentManagementService: StudentManagementService
  private readonly updateStudentScore: UpdateStudentScoreInput

  public readonly initiated: EventEmitter<void> = new EventEmitter<void>()
  public readonly chooseYearFormControl: FormControl = new FormControl()
  public readonly chooseClassTypeFormControl: FormControl = new FormControl()

  public availableYears: number[] = []
  public availableClassTypes: string[] = []
  public availableStudents: ClassStudent[] = []

  public viewMode: 'GRID' | 'DETAILS' = 'GRID'

  private selectedStudent: string | null = null

  @ViewChild('gridTemplateRef', {read: TemplateRef, static: true})
  private gridTemplateRef: TemplateRef<Component> | null = null

  @ViewChild('detailsTemplateRef', {read: TemplateRef, static: true})
  private detailsTemplateRef: TemplateRef<Component> | null = null

  @ViewChild('studentDetailsContainer', {read: ViewContainerRef, static: true})
  private studentDetailsContainer: ViewContainerRef | null = null

  constructor (@Inject(SUMMARIZE_CLASS_STUDENTS)useCase: SummarizeClassStudentsInput,
               injector: Injector,
               studentManagementService: StudentManagementService,
               @Inject(UPDATE_STUDENT_SCORE)updateStudentScore: UpdateStudentScoreInput) {
    this.injector = injector
    this.useCase = useCase
    this.view = new AngularStudentSummaryView(this)
    this.presenter = new StudentSummaryPresenter(this.useCase, this.view)
    this.studentManagementService = studentManagementService
    this.updateStudentScore = updateStudentScore
  }

  classTypeSelected (classType: string | null): void {
    this.useCase.chooseClassType(classType, this.presenter)
  }

  loaded (): void {
    this.useCase.execute(this.presenter)
  }

  yearSelected (year: string | null): void {
    this.useCase.chooseYear(yearAsNumber(year), this.presenter)
  }

  ngOnInit (): void {
    this.view.attach(this)
    this.initiated.next()
    this.studentManagementService.studentHasUpdated.subscribe((value) => {
    })
  }

  showGridMode (): void {
    this.viewMode = 'GRID'
  }

  showDetailsMode (): void {
    this.selectedStudent = null
    this.clearStudentDetails()
    this.viewMode = 'DETAILS'
  }

  getCurrentView (): TemplateRef<Component> | null {
    if (this.viewMode === 'GRID') {
      return this.gridTemplateRef
    }

    return this.detailsTemplateRef
  }

  showStudentDetails (student: ClassStudent): void {
    this.selectedStudent = student.id
    this.clearStudentDetails()
    const viewContainer: ViewContainerRef = (this.studentDetailsContainer as ViewContainerRef)
    const componentFactoryResolver: ComponentFactoryResolver = this.injector.get(ComponentFactoryResolver)
    const componentFactory: ComponentFactory<StudentDetailsComponent> = componentFactoryResolver.resolveComponentFactory(StudentDetailsComponent)
    const componentRef: ComponentRef<StudentDetailsComponent> = viewContainer.createComponent(componentFactory)
    const component: StudentDetailsComponent = componentRef.instance
    component.student = student
  }

  studentScoreUpdated ($event: number, studentId: string): void {
    const request = new UpdateStudentScoreRequest()
    request.studentId = studentId
    request.score = $event
    this.updateStudentScore.execute(request, this)
  }

  displayUpdateScoreFail (message: string): void {
    throw new Error(message)
  }

  displayUpdateScoreSuccess (student: ClassStudent): void {
    this.studentManagementService.studentHasUpdated.next(student)
  }

  isSelected (studentId: string): boolean {
    if (studentId === null) {
      return false
    }

    return this.selectedStudent === studentId
  }

  private clearStudentDetails (): void {
    (this.studentDetailsContainer as ViewContainerRef).clear()
  }
}
