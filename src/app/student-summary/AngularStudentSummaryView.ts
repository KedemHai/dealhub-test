import {
  StudentSummaryUserActions,
  StudentSummaryView
} from '../../../Dealhub/UI/StudentSummary/StudentSummaryViewContract';
import { StudentSummaryComponent } from './student-summary.component';
import { ClassStudent } from '../../../Dealhub/Domain/ClassStudent';

class AngularStudentSummaryView implements StudentSummaryView {
  private readonly component: StudentSummaryComponent;

  public constructor(component: StudentSummaryComponent) {
    this.component = component;
  }

  attach(userActions: StudentSummaryUserActions): void {
    this.component.initiated.subscribe(() => userActions.loaded());
    this.component.chooseYearFormControl.valueChanges.subscribe((value) => userActions.yearSelected(value));
    this.component.chooseClassTypeFormControl.valueChanges.subscribe((value) => userActions.classTypeSelected(value));
  }

  setAvailableClassTypeList(list: string[]): void {
    this.component.availableClassTypes = list;
  }

  setAvailableYearList(list: number[]): void {
    this.component.availableYears = list;
  }

  setClassStudentsList(list: ClassStudent[]): void {
    this.component.availableStudents = list;
  }
}

export default AngularStudentSummaryView;
