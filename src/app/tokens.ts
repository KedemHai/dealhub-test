import { InjectionToken } from '@angular/core'
import { SummarizeClassStudentsInput } from '../../Dealhub/App/SummarizeClassStudents/SummarizeClassStudentsContract'
import { ClassStudentsYearsGateway } from '../../Dealhub/App/Gateways/ClassStudentsYearsGateway'
import { ClassStudentsClassTypesGateway } from '../../Dealhub/App/Gateways/ClassStudentsClassTypesGateway'
import { ClassStudentsStudentsGateway } from '../../Dealhub/App/Gateways/ClassStudentsStudentsGateway'
import { UpdateStudentScoreInput } from '../../Dealhub/App/UpdateStudentScore/UpdateStudentScoreContract'

export const SUMMARIZE_CLASS_STUDENTS = new InjectionToken<SummarizeClassStudentsInput>('')
export const CLASS_STUDENTS_YEARS_GATEWAY = new InjectionToken<ClassStudentsYearsGateway>('')
export const CLASS_STUDENTS_CLASS_TYPES_GATEWAY = new InjectionToken<ClassStudentsClassTypesGateway>('')
export const CLASS_STUDENTS_STUDENTS_GATEWAY = new InjectionToken<ClassStudentsStudentsGateway>('')
export const UPDATE_STUDENT_SCORE = new InjectionToken<UpdateStudentScoreInput>('')
