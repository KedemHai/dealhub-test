import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
  ViewChild
} from '@angular/core'
import { fromEvent, Subject, Subscription } from 'rxjs'
import { filter, switchMapTo, take } from 'rxjs/operators'
import { FormControl } from '@angular/forms'

@Component({
  selector: 'dealhub-edit-score',
  templateUrl: './edit-score.component.html',
  styleUrls: ['./edit-score.component.scss']
})
export class EditScoreComponent implements OnInit, OnDestroy {
  @Input() public score?: number
  @Output() public updated: EventEmitter<number> = new EventEmitter<number>()
  public formControl: FormControl = new FormControl()

  private readonly elementRef: ElementRef
  private viewMode: 'TEXT' | 'EDIT' = 'TEXT'
  private editMode = new Subject()
  private editModeSubscription?: Subscription

  @ViewChild('textViewModeTemplateRef', {read: TemplateRef, static: true})
  private readonly textViewModeTemplateRef: TemplateRef<Component> | null = null

  @ViewChild('editViewModeTemplateRef', {read: TemplateRef, static: true})
  private readonly editViewModeTemplateRef: TemplateRef<Component> | null = null

  constructor (elementRef: ElementRef) {
    this.elementRef = elementRef
  }

  public ngOnInit (): void {
    this.viewModeHandler()
    this.editModeHandler()
  }

  ngOnDestroy (): void {
    if (typeof this.editModeSubscription !== 'undefined') {
      this.editModeSubscription.unsubscribe()
    }
  }

  public getViewModeTemplateRef (): TemplateRef<Component> | null {
    if (this.viewMode === 'TEXT') {
      return this.textViewModeTemplateRef
    }
    return this.editViewModeTemplateRef
  }

  private viewModeHandler (): void {
    fromEvent(this.elementRef.nativeElement, 'dblclick').subscribe(() => {
      this.formControl.setValue(this.score, {
        emitEvent: false
      })
      this.viewMode = 'EDIT'
      this.editMode.next(true)
    })
  }

  private editModeHandler (): void {
    const clickedOutside$ = fromEvent(document, 'click').pipe(
      filter(({target}) => !(this.elementRef.nativeElement as Element).contains(target as Node)),
      take(1),
    )

    this.editModeSubscription = this.editMode.pipe(
      switchMapTo(clickedOutside$)
    ).subscribe(() => {
      this.updated.next(this.formControl.value)
      this.viewMode = 'TEXT'
    })
  }
}
