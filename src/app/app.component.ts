import { Component } from '@angular/core';

@Component({
  selector: 'dealhub-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dealhub';
}
