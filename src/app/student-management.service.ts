import { Injectable } from '@angular/core'
import { ClassStudent } from '../../Dealhub/Domain/ClassStudent'
import { ReplaySubject, Subject } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class StudentManagementService {
  public readonly studentHasUpdated: Subject<ClassStudent> = new ReplaySubject<ClassStudent>(1)
}
