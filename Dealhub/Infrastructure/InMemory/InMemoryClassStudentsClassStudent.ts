import { ClassStudent } from '../../Domain/ClassStudent'
import { InMemoryClassStudentsItemData, InMemoryClassStudentsStudentData } from './InMemoryClassStudentsContract'

class InMemoryClassStudentsClassStudent implements ClassStudent {
  private readonly studentData: InMemoryClassStudentsStudentData
  private readonly classTypeData: InMemoryClassStudentsItemData['classType']

  public constructor (studentData: InMemoryClassStudentsStudentData, classTypeData: InMemoryClassStudentsItemData['classType']) {
    this.studentData = studentData
    this.classTypeData = classTypeData
  }

  public get classType (): string {
    return this.classTypeData
  }

  public get fName (): string {
    return this.studentData.fname
  }

  public get grade (): number {
    return this.studentData.grade
  }

  public get id (): string {
    return `${this.classType}-${this.year}-${this.fName}-${this.lName}`
  }

  public get lName (): string {
    return this.studentData.lname
  }

  public get year (): number {
    return this.studentData.year
  }

  public setScore (score: number): void {
    this.studentData.grade = score
  }
}

export default InMemoryClassStudentsClassStudent
