import { ClassStudentsYearsGateway } from '../../App/Gateways/ClassStudentsYearsGateway'
import InMemoryClassStudents from './InMemoryClassStudents'

class InMemoryClassStudentsYearsGateway implements ClassStudentsYearsGateway {
  private readonly inMemoryClassStudents: InMemoryClassStudents = new InMemoryClassStudents()
  private cacheAllYears: number[] | null = null
  private cacheYearsByClassType: Map<string, number[]> = new Map<string, number[]>()

  public async findAllYears (): Promise<number[]> {
    if (this.cacheAllYears === null) {
      const data = this.inMemoryClassStudents.getData()
      this.cacheAllYears = data.classStudents.reduce<number[]>((foundYears, classStudents) => {
        for (const classStudent of classStudents.students) {
          const currentYear = classStudent.year
          const exist = foundYears.find(value => value === currentYear)
          if (typeof exist === 'undefined') {
            foundYears.push(currentYear)
          }
        }

        return foundYears
      }, [])
    }

    return this.cacheAllYears
  }

  public async findYearsByClassType (classType: string): Promise<number[]> {
    const cached = this.cacheYearsByClassType.get(classType)
    if (typeof cached === 'undefined') {
      const data = this.inMemoryClassStudents.getData()
      const foundClassType = data.classStudents.find(value => value.classType === classType)
      if (typeof foundClassType === 'undefined') {
        this.cacheYearsByClassType.set(classType, [])
      } else {
        const yearsByClassType = foundClassType.students.reduce<number[]>((foundYears, student) => {
          const currentYear = student.year
          const exist = foundYears.find(value => value === currentYear)
          if (typeof exist === 'undefined') {
            foundYears.push(currentYear)
          }

          return foundYears
        }, [])

        this.cacheYearsByClassType.set(classType, yearsByClassType)
      }
    }

    return this.cacheYearsByClassType.get(classType) as number[]
  }
}

export default InMemoryClassStudentsYearsGateway
