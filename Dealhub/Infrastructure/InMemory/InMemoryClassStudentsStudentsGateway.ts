import {
  ClassStudentsStudentsGateway,
  ClassStudentsStudentsGatewayStudentRequest
} from '../../App/Gateways/ClassStudentsStudentsGateway'
import { ClassStudent } from '../../Domain/ClassStudent'
import InMemoryClassStudents from './InMemoryClassStudents'
import InMemoryClassStudentsClassStudent from './InMemoryClassStudentsClassStudent'

interface SearchableDataStructure {
  student: ClassStudent
  classType: string
  year: number
}

function queryBuilder (request: ClassStudentsStudentsGatewayStudentRequest): { [key in keyof SearchableDataStructure]?: any } {
  const dataQuery: { [key in keyof SearchableDataStructure]?: any } = {}
  if (typeof request.classType !== 'undefined') {
    dataQuery.classType = request.classType
  }
  if (typeof request.year !== 'undefined') {
    dataQuery.year = request.year
  }

  return dataQuery
}

class InMemoryClassStudentsStudentsGateway implements ClassStudentsStudentsGateway {
  private readonly inMemoryClassStudents: InMemoryClassStudents = new InMemoryClassStudents()
  private searchableDataStructures: SearchableDataStructure[] | null = null

  public async findStudentsByRequest (request: ClassStudentsStudentsGatewayStudentRequest): Promise<ClassStudent[]> {
    this.mapToDataStructure()

    const dataQuery = queryBuilder(request)
    const results = (this.searchableDataStructures as SearchableDataStructure[]).filter(value => {
      return Object.keys(dataQuery).every((key) => dataQuery[key as keyof SearchableDataStructure] === value[key as keyof SearchableDataStructure])
    })

    return results.map<ClassStudent>(value => value.student)
  }

  private mapToDataStructure (): void {
    if (this.searchableDataStructures !== null) {
      return
    }

    const data = this.inMemoryClassStudents.getData()
    this.searchableDataStructures = data.classStudents.reduce<SearchableDataStructure[]>((dataStructures, dataItem) => {
      for (const student of dataItem.students) {
        dataStructures.push({
          classType: dataItem.classType,
          year: student.year,
          student: new InMemoryClassStudentsClassStudent(student, dataItem.classType)
        })
      }
      return dataStructures
    }, [])
  }

  public async getStudentById (id: string): Promise<ClassStudent> {
    this.mapToDataStructure()

    const found = (this.searchableDataStructures as SearchableDataStructure[]).find((value) => value.student.id === id)
    if (typeof found === 'undefined') {
      throw new Error()
    }

    return found.student
  }

  public async saveStudent (student: ClassStudent): Promise<void> {
    this.mapToDataStructure()

    const findStudent = (this.searchableDataStructures as SearchableDataStructure[]).findIndex((value) => {
      return value.student.id === student.id
    })
    if (findStudent < 0) {
      throw new Error('Student not found')
    }

    (this.searchableDataStructures as SearchableDataStructure[])[findStudent].student = student
    return Promise.resolve()
  }
}

export default InMemoryClassStudentsStudentsGateway
