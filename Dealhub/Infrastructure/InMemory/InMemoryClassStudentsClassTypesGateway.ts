import { ClassStudentsClassTypesGateway } from '../../App/Gateways/ClassStudentsClassTypesGateway'
import InMemoryClassStudents from './InMemoryClassStudents'

class InMemoryClassStudentsClassTypesGateway implements ClassStudentsClassTypesGateway {
  private readonly inMemoryClassStudents: InMemoryClassStudents = new InMemoryClassStudents()
  private cacheClassTypeByYear: Map<string, string[]> = new Map<string, string[]>()

  public async findAllClassTypes (): Promise<string[]> {
    const data = this.inMemoryClassStudents.getData()
    return data.classTypes
  }

  public async findClassTypesByYear (year: number): Promise<string[]> {
    const cacheKey = String(year)
    const cached = this.cacheClassTypeByYear.get(cacheKey)
    if (typeof cached === 'undefined') {
      const data = this.inMemoryClassStudents.getData()
      const result = data.classStudents.reduce<string[]>((foundClassTypes, classStudentsItem) => {
        if (classStudentsItem.students.some((student) => student.year === year)) {
          if (!foundClassTypes.some(value => value === classStudentsItem.classType)) {
            foundClassTypes.push(classStudentsItem.classType)
          }
        }

        return foundClassTypes
      }, [])

      this.cacheClassTypeByYear.set(cacheKey, result)
    }

    return this.cacheClassTypeByYear.get(cacheKey) as string[]
  }
}

export default InMemoryClassStudentsClassTypesGateway
