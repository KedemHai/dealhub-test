interface InMemoryClassStudentsData {
  classStudents: InMemoryClassStudentsItemData[]
  classTypes: string[]
}

interface InMemoryClassStudentsItemData {
  students: InMemoryClassStudentsStudentData[]
  classType: string
}

interface InMemoryClassStudentsStudentData {
  fname: string
  lname: string
  grade: number
  year: number
}

export { InMemoryClassStudentsData, InMemoryClassStudentsItemData, InMemoryClassStudentsStudentData }
