import { ClassStudent } from '../../Domain/ClassStudent'

interface StudentSummaryUserActions {
  loaded (): void

  yearSelected (year: string | null): void

  classTypeSelected (classType: string | null): void
}

interface StudentSummaryView {
  attach (userActions: StudentSummaryUserActions): void

  setClassStudentsList (list: ClassStudent[]): void

  setAvailableYearList (list: number[]): void

  setAvailableClassTypeList (list: string[]): void
}

export { StudentSummaryUserActions, StudentSummaryView }
