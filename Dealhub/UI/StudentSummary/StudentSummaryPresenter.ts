import { StudentSummaryView } from './StudentSummaryViewContract'
import {
  SummarizeClassStudentsInput,
  SummarizeClassStudentsOutput
} from '../../App/SummarizeClassStudents/SummarizeClassStudentsContract'
import { ClassStudent } from '../../Domain/ClassStudent'

class StudentSummaryPresenter implements SummarizeClassStudentsOutput {
  private readonly summarizeClassStudents: SummarizeClassStudentsInput
  private readonly view: StudentSummaryView

  public constructor (summarizeClassStudents: SummarizeClassStudentsInput, view: StudentSummaryView) {
    this.summarizeClassStudents = summarizeClassStudents
    this.view = view
  }

  public displayAvailableClassTypes (classTypes: string[]): void {
    this.view.setAvailableClassTypeList(classTypes)
  }

  public displayAvailableYears (years: number[]): void {
    this.view.setAvailableYearList(years)
  }

  public displayStudentSummary (summary: ClassStudent[]): void {
    this.view.setClassStudentsList(summary)
  }
}

export default StudentSummaryPresenter
