import { ClassStudent } from '../../Domain/ClassStudent'

interface ClassStudentsStudentsGateway {
  findStudentsByRequest (request: ClassStudentsStudentsGatewayStudentRequest): Promise<ClassStudent[]>

  getStudentById (id: string): Promise<ClassStudent>

  saveStudent (student: ClassStudent): Promise<void>
}

interface ClassStudentsStudentsGatewayStudentRequest {
  year?: number
  classType?: string;
}

export { ClassStudentsStudentsGateway, ClassStudentsStudentsGatewayStudentRequest }
