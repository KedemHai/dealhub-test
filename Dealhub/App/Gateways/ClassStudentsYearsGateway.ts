interface ClassStudentsYearsGateway {
  findAllYears (): Promise<number[]>

  findYearsByClassType (classType: string): Promise<number[]>
}

export { ClassStudentsYearsGateway }
