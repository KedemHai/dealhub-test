interface ClassStudentsClassTypesGateway {
  findAllClassTypes (): Promise<string[]>

  findClassTypesByYear (year: number): Promise<string[]>
}

export { ClassStudentsClassTypesGateway }
