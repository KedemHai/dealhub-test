import { ClassStudent } from '../../Domain/ClassStudent'

interface SummarizeClassStudentsInput {
  execute (output: SummarizeClassStudentsOutput): void

  chooseYear (year: number | null, output: SummarizeClassStudentsOutput): void

  chooseClassType (classType: string | null, output: SummarizeClassStudentsOutput): void
}

interface SummarizeClassStudentsOutput {
  displayAvailableYears (years: number[]): void

  displayAvailableClassTypes (classTypes: string[]): void

  displayStudentSummary (summary: ClassStudent[]): void
}

export { SummarizeClassStudentsInput, SummarizeClassStudentsOutput }
