import { SummarizeClassStudentsInput, SummarizeClassStudentsOutput } from './SummarizeClassStudentsContract'
import { ClassStudentsYearsGateway } from '../Gateways/ClassStudentsYearsGateway'
import { ClassStudentsClassTypesGateway } from '../Gateways/ClassStudentsClassTypesGateway'
import {
  ClassStudentsStudentsGateway,
  ClassStudentsStudentsGatewayStudentRequest
} from '../Gateways/ClassStudentsStudentsGateway'

class SummarizeClassStudents implements SummarizeClassStudentsInput {
  private readonly yearsGateway: ClassStudentsYearsGateway
  private readonly classTypesGateway: ClassStudentsClassTypesGateway
  private readonly classStudentsGateway: ClassStudentsStudentsGateway
  private currentYear: null | number = null
  private currentClassType: null | string = null

  public constructor (yearsGateway: ClassStudentsYearsGateway, classTypesGateway: ClassStudentsClassTypesGateway, classStudentsGateway: ClassStudentsStudentsGateway) {
    this.yearsGateway = yearsGateway
    this.classTypesGateway = classTypesGateway
    this.classStudentsGateway = classStudentsGateway
  }

  public async chooseClassType (classType: string | null, output: SummarizeClassStudentsOutput): Promise<void> {
    this.currentClassType = classType
    await this.displayAvailableYears(output)
    await this.displayClassStudents(output)
  }

  public async chooseYear (year: number | null, output: SummarizeClassStudentsOutput): Promise<void> {
    this.currentYear = year
    await this.displayAvailableClassTypes(output)
    await this.displayClassStudents(output)
  }

  public async execute (output: SummarizeClassStudentsOutput): Promise<void> {
    await this.displayAvailableYears(output)
    await this.displayAvailableClassTypes(output)
    await this.displayClassStudents(output)
  }

  private async displayAvailableYears (output: SummarizeClassStudentsOutput): Promise<void> {
    let availableYears: number[]
    if (this.currentClassType === null) {
      availableYears = await this.yearsGateway.findAllYears()
    } else {
      availableYears = await this.yearsGateway.findYearsByClassType(this.currentClassType)
    }
    output.displayAvailableYears(availableYears)
  }

  private async displayAvailableClassTypes (output: SummarizeClassStudentsOutput): Promise<void> {
    let availableClassTypes: string[]
    if (this.currentYear === null) {
      availableClassTypes = await this.classTypesGateway.findAllClassTypes()
    } else {
      availableClassTypes = await this.classTypesGateway.findClassTypesByYear(this.currentYear)
    }
    output.displayAvailableClassTypes(availableClassTypes)
  }

  private async displayClassStudents (output: SummarizeClassStudentsOutput): Promise<void> {
    const request: ClassStudentsStudentsGatewayStudentRequest = {}
    if (this.currentYear !== null) {
      request.year = this.currentYear
    }
    if (this.currentClassType !== null) {
      request.classType = this.currentClassType
    }

    const allStudents = await this.classStudentsGateway.findStudentsByRequest(request)
    output.displayStudentSummary(allStudents)
  }
}

export default SummarizeClassStudents
