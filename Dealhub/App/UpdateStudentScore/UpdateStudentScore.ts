import { UpdateStudentScoreInput, UpdateStudentScoreOutput } from './UpdateStudentScoreContract'
import { ClassStudentsStudentsGateway } from '../Gateways/ClassStudentsStudentsGateway'
import UpdateStudentScoreRequest from './UpdateStudentScoreRequest'

class UpdateStudentScore implements UpdateStudentScoreInput {
  private readonly studentsGateway: ClassStudentsStudentsGateway

  public constructor (studentsGateway: ClassStudentsStudentsGateway) {
    this.studentsGateway = studentsGateway
  }

  public async execute (request: UpdateStudentScoreRequest, output: UpdateStudentScoreOutput): Promise<void> {
    if (typeof request.studentId === 'undefined') {
      return output.displayUpdateScoreFail('Student Id invalid')
    }
    if (typeof request.score === 'undefined') {
      return output.displayUpdateScoreFail('Score is invalid')
    }
    try {
      const student = await this.studentsGateway.getStudentById(request.studentId)
      student.setScore(request.score)
      await this.studentsGateway.saveStudent(student)
      output.displayUpdateScoreSuccess(student)
    } catch (e) {
      output.displayUpdateScoreFail((e as Error).message)
    }
  }
}

export default UpdateStudentScore
