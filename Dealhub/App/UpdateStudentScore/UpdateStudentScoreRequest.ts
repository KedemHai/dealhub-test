class UpdateStudentScoreRequest {
  public studentId?: string
  public score?: number
}

export default UpdateStudentScoreRequest
