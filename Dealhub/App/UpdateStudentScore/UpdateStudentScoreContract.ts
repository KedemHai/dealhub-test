import UpdateStudentScoreRequest from './UpdateStudentScoreRequest'
import { ClassStudent } from '../../Domain/ClassStudent'

interface UpdateStudentScoreInput {
  execute (request: UpdateStudentScoreRequest, output: UpdateStudentScoreOutput): void
}

interface UpdateStudentScoreOutput {
  displayUpdateScoreSuccess (student: ClassStudent): void

  displayUpdateScoreFail (message: string): void
}

export { UpdateStudentScoreInput, UpdateStudentScoreOutput }
