interface ClassStudent {
  id: string
  fName: string
  lName: string
  classType: string
  grade: number
  year: number

  setScore (score: number): void
}

export { ClassStudent }
